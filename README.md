# Mental-Health-Web-App

## Description
This is the web application for doctor. Doctor will login through this web application to monitor patients and add additional activities for patients to do.

## Getting started
`npm start` to start the web application. The apps will run on port 8080 by default.

## Pages
- Login
- Register
- Home
- List of Patients
- Manage Patient
- Patient Details
- Create Questionnaire
- Settings

## Features and Use Cases
- Doctor can login in `LoginPage` using `email` and `password`
- Doctor can register in `RegisterPage` by indicationg `email`, `username`, `password`, `hopsital` and `gender`
- `HomePage` will display the latest activity done by the doctor's patients sorted by `timestamp`, only top 5 activities will be displayed
- `ListofPatientsPage` will display the list of patients subscribed by doctor. Pressing `Details` button will display the latest activity of the patient. Clicking the name of the patient on the top will bring doctor to the patient's details.
- `ManagePatientPage` enable doctor to subscribe or unsubscribe to patients. Autocompletebox will get the name from index of patient's name in `Patient` schema
- `PatientDetailsPage` will display the basic information of the patients which are stored in `Patient` Schema. Also, there are some custom graph that will display the statistics of the ativity done by patients over the course of the year, month or week. Each graph will be explained further on the next section.
- `CreateQuestionnairePage` enable doctors to create custom questionnaire for students. Questionnaire will be stored on `Questionnaire` Schema. Each question can be multiple-choice or fill in the blank according to doctors. `Questionnaire` will be send to patients app. Each questionnaire has a name. Multiple choice max five choices.
- `SettingsPage` in which the doctor can change the details of the doctor.

## Patient Graph
Patient can do many activities daily for doctor. We see that having many different kinds of graphs will be beneficial for doctor. The bare minimum things that doctor can observe is that the data can be seen by daily, weekly, monthly.
Here are the details of the graph of patients:
- `Daily emotion`: Pie Chart which is divided by emotion's response
- `Shape Activity`: Display the latest shape and also if clicked can show the history of shape picked by patient
- `Color Activity`: Pie Chart which is divided by color choice
- `Picture Activity`: Display the latest picture and also if clicked can show the history of picture picked by patient
- `Drawing Activit`: Display the latest drawing and also if clicked can show the history of drawing done by patient
- `Questionnaire Answer`: Show the result of questionnaire by patient and can display custom questionnaire

## Testing
Testing is done using `Jest` and `Enzyme`

## TODO
- [ ] Add icon for profile and name of doctor
- [ ] Add pages to forgot password
- [ ] Change icon for web application
- [ ] Add ESLint and Prettier for better code style
- [ ] Add test code in `Jest` and `Enzyme`
- [ ] Add babel for ES6 support
- [ ] Add webpack if necessary

## Contact
For questions and inqueries about this repository, please email `andreveeguil@gmail.com`
