import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span>Doctor Monitoring</span>
      </footer>
    )
  }
}

export default Footer;
