import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Collapse, Button, Fade
} from 'reactstrap';

class PatientCard extends Component {

  constructor(props) {
      super(props);
      this.onEntering = this.onEntering.bind(this);
      this.onEntered = this.onEntered.bind(this);
      this.onExiting = this.onExiting.bind(this);
      this.onExited = this.onExited.bind(this);
      this.toggle = this.toggle.bind(this);
      this.toggleFade = this.toggleFade.bind(this);
      this.state = {
        collapse: false,
        status: 'Closed',
        fadeIn: true,
        timeout: 300
      };
    }
  
  onEntering() {
      this.setState({status: 'Opening...'});
  }

  onEntered() {
      this.setState({status: 'Opened'});
  }

  onExiting() {
      this.setState({status: 'Closing...'});
  }

  onExited() {
      this.setState({status: 'Closed'});
  }

  toggle() {
      this.setState({collapse: !this.state.collapse});
  }

  toggleFade() {
      this.setState({fadeIn: !this.state.fadeIn});
  }
          
  render() {
      return (
          <Card>
              <CardHeader>
                  <Link to={"/patientDetails" + this.props.name}><strong>Patient {this.props.name} </strong></Link>
              </CardHeader>
              <CardFooter>
                  <Button color="primary" onClick={this.toggle} style={{marginBottom: '1rem'}}>Details</Button>
              </CardFooter>
              <Collapse
                  isOpen={this.state.collapse}
                  onEntering={this.onEntering}
                  onEntered={this.onEntered}
                  onExiting={this.onExiting}
                  onExited={this.onExited}
              >
                  <CardBody>
                      <p>Patient Felix said he is happy today.</p>
                      <p>Patient Felix rates his happiness 3 out of 5</p>
                  </CardBody>
              </Collapse>
          </Card>
      );
  }
}

export default PatientCard;
