export default {
  items: [
    {
      name: 'Home',
      url: '/dashboard',
      icon: 'icon-home',
    },
    {
      name: 'List of Patients',
      url: '/listPatient',
      icon: 'icon-list',
    },
    {
      name: 'Manage Patient',
      url: '/managePatient',
      icon: 'icon-wrench',
    },
    {
      name: 'Patient Details',
      url: '/patientDetails',
      icon: 'icon-note',
    },
    {
      name: 'Create Questionnaire',
      url: '/questionnaire',
      icon: 'icon-puzzle',
    },
    {
      name: 'Settings',
      url: '/settings',
      icon: 'icon-settings',
    }
  ]
};
