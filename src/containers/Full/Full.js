import React, {Component} from 'react';
import {Link, Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Aside from '../../components/Aside/';
import Footer from '../../components/Footer/';

import Dashboard from '../../views/Dashboard/';

import Charts from '../../views/Charts';
import ListPatients from '../../views/ListPatients';
import ManagePatient from '../../views/ManagePatient';
import PatientDetails from '../../views/PatientDetails';
import CreateQuestionnaire from '../../views/CreateQuestionnaire';
import Settings from '../../views/Settings';

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              <Switch>
                <Route path="/dashboard" name="Dashboard" component={Dashboard}/>
                <Route exact path="/managePatient" component={ManagePatient} />
                <Route exact path="/patientDetails" component={PatientDetails} />
                <Route exact path="/questionnaire" component={CreateQuestionnaire} />
                <Route exact path="/settings" component={Settings} />
                <Route exact path="/listPatient" name="List of Patients" component={ListPatients} />
                <Redirect from="/" to="/dashboard"/>
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Full;
