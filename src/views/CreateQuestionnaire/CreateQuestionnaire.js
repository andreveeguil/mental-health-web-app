import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Row, Col, Card, CardHeader, CardBody, CardFooter,  ButtonDropdown, DropdownToggle,
        DropdownMenu, DropdownItem, Button, FormGroup, Label, Input, FormText } from 'reactstrap';

class Question extends Component{
  constructor(props){
    super(props)
    this.state = {
      checked : true
    }
    console.log("Matamu")
    this.changeFormat = this.changeFormat.bind(this)
  }

  changeFormat(){
    console.log("Called");
    const elementId = "multipleChoice" + this.props.number.toString()
    console.log(elementId)
    if (document.getElementById(elementId).checked === true ){
      console.log("mantap"); 
      ReactDOM.render(
        <div>
          <FormGroup check inline>
            <Input className="form-check-input" type="radio" id="inline-radio1-1" name="inline-radios-question" value="option1"/>
            <Label className="form-check-label" check htmlFor="inline-radio1">Multiple choices</Label>
          </FormGroup>
          <FormGroup check inline>
            <Input className="form-check-input" type="radio" id="inline-radio2" name="inline-radios-question" value="option2"/>
            <Label className="form-check-label" check htmlFor="inline-radio2-1">Fill in the blank</Label>
          </FormGroup>
        </div>,
        document.getElementById("answerFormat" + this.props.number.toString())
      )
    }
    else {
      console.log("Tidak mantap");
      ReactDOM.render(
        <FormGroup row>
          <Col md="3">
            <Label htmlFor="text-input">Answer</Label>
          </Col>
          <Col xs="12" md="9">
            <Input type="text" id="text-input" name="text-input" placeholder="Text"/>
            <FormText color="muted">This is a help text</FormText>
          </Col>
        </FormGroup>,
        document.getElementById("answerFormat" + this.props.number.toString())
      )
    }
  }

  render(){
    return(
      <Col xs="12">
        <Card>
          <CardHeader>
            Question 1
          </CardHeader>
          <CardBody>
            <FormGroup row>
              <Col md="3">
                <Label htmlFor="textarea-input">Textarea</Label>
              </Col>
              <Col xs="12" md="9">
                <Input type="textarea" name="textarea-input" id="textarea-input" rows="9"
                        placeholder="Content..."/>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col md="3">
                <Label>Question format</Label>
              </Col>
              <Col md="9">
                <FormGroup check inline>
                  <Input className="form-check-input" onClick={this.changeFormat} type="radio" name= { "questionChoice" + this.props.number.toString()}
                  id= {"multipleChoice" + this.props.number.toString()}
                  value="option1"/>
                  <Label className="form-check-label" check htmlFor={"multipleChoice" + this.props.number.toString()}>Multiple choices</Label>
                </FormGroup>
                <FormGroup check inline>
                  <Input className="form-check-input" onClick={this.changeFormat} type="radio" name= { "questionChoice" + this.props.number.toString()}
                  id= {"fillInTheBlank" + this.props.number.toString()} 
                  value="option2"/>
                  <Label className="form-check-label" check htmlFor={"multipleChoice" + this.props.number.toString()}>Fill in the blank</Label>
                </FormGroup>
              </Col>
            </FormGroup>
            <div id={"answerFormat" + this.props.number.toString()}>
            </div>
          </CardBody>
        </Card>
      </Col>
        // <label>
        //   <input type="radio" checked = {this.state.checked}
        //     onClick = {this.changeFormat} name= { "questionChoice" + this.props.number.toString()}
        //     id= {"multipleChoice" + this.props.number.toString()} />
        //   <label for="multipleChoice">Multiple Choice</label>
        //   <input type="radio" checked = {!this.state.checked}
        //     onClick = {this.changeFormat} name= { "questionChoice" + this.props.number.toString()}
        //     id= {"fillInTheBlank" + this.props.number.toString()} />
        //   <label for="fillInTheBlank">Fill in the blank</label>
        //   <br />
        //   <div id= {"answerFormat" + this.props.number.toString()}>
        //   </div>
        // </label>
    )
  }
}

class CreateQuestionnaire extends Component {

  constructor(props){
    super(props)
    this.state = {
      numQuestions : []
    }
    this.addQuestion = this.addQuestion.bind(this)
  }

  addQuestion(){
    var dummyList = []
    var dummyList = this.state.numQuestions
    dummyList.push(this.state.numQuestions.length + 1)
    this.setState({
      numQuestions : dummyList
    })
    console.log("Halo");
    const listQuestion = dummyList.map((dummy) =>
      <li><Question number= {dummy} /></li>
    )
    console.log("Matamu");
    ReactDOM.render(
      <ol>{listQuestion}</ol>,
      document.getElementById('questionList')
    )
    console.log("Besar");
  }

  render() {
    return (
      <div className="animated fadeIn">
        <button class="btn btn-light" onClick={this.addQuestion}>Add questions</button>
        <Row>
          <div id="questionList">
          </div>
          {/* <Col xs="12">
            <Card>
              <CardHeader>
                Question 1
              </CardHeader>
              <CardBody>
                What is your condition the day after tomorrow? <br />
              </CardBody>
            </Card>
          </Col> */}
        </Row>
      </div>
    )
  }
}

export default CreateQuestionnaire;
