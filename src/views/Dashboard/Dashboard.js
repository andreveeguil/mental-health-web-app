import React, {Component} from 'react';
import PatientCard from '../../components/PatientCard';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctor_email: "andreveeguil@gmail.com",
      patients: [],
      isDataFetch: false
    }
  }

  componentDidMount() {
    fetch('http://localhost:3000/cachingDoctor/email/' + this.state.doctor_email)
    .then(response => response.json())
    .then(jsonResponse => this.setState( {
      patients: jsonResponse,
      isDataFetch: true
    } ));
  }

  render() {
    if(!this.state.isDataFetch) return null;
    var patientList = [];
    for (let i=0; i<this.state.patients.length; i++){
      patientList.push(<PatientCard name={this.state.patients[i]['patientEmail']} />);
    }
    return (
      <div className="animated fadeIn">
        {patientList}
      </div>
    );
  }
}

export default Dashboard;
