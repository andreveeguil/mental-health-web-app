import React, { Component } from 'react';

import PatientCard from '../../components/PatientCard';

class ListPatients extends Component {

  constructor(props) {
    super(props);
    this.state = {
      doctor_email: "andreveeguil@gmail.com",
      patients: [],
      patientsActivity: [],
      isDataFetch: false
    }
  }

  componentDidMount() {
    fetch('http://localhost:3000/doctorSubscription/email/' + this.state.doctor_email)
    .then(response => response.json())
    .then(jsonResponse => this.setState({
      patients: jsonResponse,
    }));

    for (index in this.state.patients) {
      patientEmail = this.state.patients[index];
      fetch('http://localhost:3000/cachingPatient/email' + patientEmail)
      .then(response => response.json())
      .then(jsonResponse => this.state.patientsActivity.push(jsonResponse))
    }
    this.setState({ isDataFetch: true});
  }

  render() {
    if(!this.state.isDataFetch) return null;
    var patientList = [];
    for (let i=0; i<this.state.patients.length; i++){
      patientList.push(<PatientCard name={this.state.patients[i]['patientEmail']} />);
    }
    return (
      <div className="animated fadeIn">
        {patientList}
      </div>
    );
  }
}

export default ListPatients;
