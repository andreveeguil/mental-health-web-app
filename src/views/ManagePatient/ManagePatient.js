import React, {Component} from 'react';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Collapse, Button, Fade,
} from 'reactstrap';
import { AutoComplete } from 'material-ui';
import RaisedButton from 'material-ui/RaisedButton'
import getMuiTheme        from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider   from 'material-ui/styles/MuiThemeProvider';

class ManagePatient extends Component {

  constructor(props) {
    super(props);
    this.state = {
      patientEmail: "andreveeguil@gmail.com",
      doctorEmail: "andreveeguil@gmail.com",
      collapse: false,
      status: 'Closed',
      fadeIn: true,
      timeout: 300,
      dataSource : ["Datanglah", "Kepadaku"], 
    };
    this.onEntering = this.onEntering.bind(this);
    this.onEntered = this.onEntered.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.onSubscribe = this.onSubscribe.bind(this);
    this.onUnsubscribe = this.onUnsubscribe.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
  }

  onSubscribe() {
    var doctorSubscriptionId = this.state.doctorEmail + ':' + this.state.patientEmail;
    console.log(doctorSubscriptionId);
    fetch('http://localhost:3000/doctorSubscription/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        doctorSubscriptionId: doctorSubscriptionId,
        doctorEmail: this.state.doctorEmail,
        patientEmail: this.state.patientEmail
      })
    })
    .then((response) => {
      console.log(response)
      alert("Subscription is confirmed");
    })
  }

  onUnsubscribe() {
    var doctorSubscriptionId = this.state.doctorEmail + ':' + this.state.patientEmail;
    console.log(doctorSubscriptionId);
    fetch('http://localhost:3000/doctorSubscription/', {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response) => {
      console.log(response)
      alert("Subscription is deleted");
    })
  }

  onChangeEmail() {
    this.setState({ patientEmail: event.target.value});
  }

  onEntering() {
    this.setState({status: 'Opening...'});
  }

  onEntered() {
    this.setState({status: 'Opened'});
  }

  onExiting() {
    this.setState({status: 'Closing...'});
  }

  onExited() {
    this.setState({status: 'Closed'});
  }

  toggle() {
    this.setState({collapse: !this.state.collapse});
  }

  toggleFade() {
    this.setState({fadeIn: !this.state.fadeIn});
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Card>
          <CardHeader>
            <MuiThemeProvider muiTheme={getMuiTheme()}>
              <AutoComplete
              dataSource = {this.state.dataSource}
              onUpdateInput = {this.onUpdateInput}
              onChange = {this.onChangeEmail} />
            </MuiThemeProvider>
            <br />
            <Button color="success" onClick={this.onSubscribe}>Subscribe</Button>
            <Button color="primary" onClick={this.onSubscribe}>Unsubscribe</Button>
          </CardHeader>
        </Card>
      </div>
    );
  }
}

export default ManagePatient;