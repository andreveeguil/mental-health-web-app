import React, {Component} from 'react';
import { Redirect } from 'react-router-dom';
import {Container, Row, Col, Card, CardBody, CardFooter, Button, Input, InputGroup, InputGroupAddon} from 'reactstrap';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "default",
      email: "default@gmail.com",
      password: "123123",
      passwordConf: "123123",
      hospital: "Montgomerry",
      gender: "Male",
      redirect: false
    }
    this.register = this.register.bind(this);
    this.handleUserNameChange = this.handleUserNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handlePasswordConfChange = this.handlePasswordConfChange.bind(this);
    this.handleHospitalChange = this.handleHospitalChange.bind(this);
    this.handleGenderChange = this.handleGenderChange.bind(this);
  }

  register() {
    console.log("Everything's okay");
    console.log(this.state);
    fetch('http://localhost:3000/doctor/register', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body:  JSON.stringify(this.state)
    })
      .then((result) => {
        if(result) {
          this.state.redirect = true;
        }
      })
  }

  handleUserNameChange(event) {
    this.setState({ username: event.target.value});
    console.log(this.state.username);
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value});
    console.log(this.state.email);
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value});
    console.log(this.state.password);
  }

  handlePasswordConfChange(event) {
    this.setState({ passwordConf: event.target.value});
    console.log(this.state.passwordConf);
  }

  handleHospitalChange(event) {
    this.setState({ hospital: event.target.value});
    console.log(this.state.hospital);
  }

  handleGenderChange(event) {
    this.setState({ gender: event.target.value});
    console.log(this.state.gender);
  }

  render() {
    const { redirect } = this.state;
    if (redirect) {
      console.log("Redirecting");
      return <Redirect to="/dashboard" />;
    }

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <h1>Register</h1>
                  <p className="text-muted">Create your account</p>
                  <InputGroup className="mb-3">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="icon-user"></i>
                      </span>
                    </div>
                    <Input type="text" onChange={this.handleUserNameChange} placeholder="Username"/>
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <div className="input-group-prepend">
                      <span className="input-group-text">@</span>
                    </div>
                    <Input type="text" onChange={this.handleEmailChange} placeholder="Email"/>
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="icon-lock"></i>
                      </span>
                    </div>
                    <Input type="password" onChange={this.handlePasswordChange} placeholder="Password"/>
                  </InputGroup>
                  <InputGroup className="mb-4">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="icon-lock"></i>
                      </span>
                    </div>
                    <Input type="password" onChange={this.handlePasswordConfChange} placeholder="Repeat password"/>
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <Input type="text" onChange={this.handleHospitalChange} placeholder="Hospital"/>
                  </InputGroup>
                  Please select your gender: <br />
                  <input type="radio" onChange={this.handleGenderChange} name="gender" value="Male" /> Male<br />
                  <input type="radio" onChange={this.handleGenderChange} name="gender" value="Female" /> Female<br /><br />
                  <Button color="success" onClick={this.register} block>Create Account</Button>
                </CardBody>
                <CardFooter className="p-4">
                  <Row>
                    <Col xs="12" sm="6">
                      <Button className="btn-facebook" block><span>facebook</span></Button>
                    </Col>
                    <Col xs="12" sm="6">
                      <Button className="btn-twitter" block><span>twitter</span></Button>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Register;
