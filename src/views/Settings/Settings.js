import React, {Component} from 'react';
import {
  Row,
  Col,
  Button,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
} from 'reactstrap';

class Settings extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      collapse: true,
      username: "andreveeguil",
      email: "andreveeguil@gmail.com",
      password: "123123",
      passwordConf: "123123",
      hospital: "Montgomerry",
      gender: "Male",
      personalComment: "No comment"
    };

    this.toggle = this.toggle.bind(this);
    this.updateChanges = this.updateChanges.bind(this);
    this.handleUserNameChange = this.handleUserNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleHospitalChange = this.handleHospitalChange.bind(this);
    this.handleGenderChange = this.handleGenderChange.bind(this);
    this.handlePersonalCommentChange = this.handlePersonalCommentChange.bind(this);
  }

  updateChanges() {
    console.log(this.state);
    fetch('http://localhost:3000/doctor/updateParticular',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body:  JSON.stringify({
        username: this.state.username,
        email: this.state.email,
        password: this.state.password,
        hospital: this.state.hospital,
        gender: this.state.gender,
        personalComment: this.state.personalComment
      })
    })
    .then(response => alert("Done updating"))
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  handleUserNameChange(event) {
    this.setState({ username: event.target.value});
    console.log(this.state.username);
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value});
    console.log(this.state.email);
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value});
    console.log(this.state.password);
  }

  handleHospitalChange(event) {
    this.setState({ hospital: event.target.value});
    console.log(this.state.hospital);
  }

  handleGenderChange(event) {
    this.setState({ gender: event.target.value});
    console.log(this.state.gender);
  }

  handlePersonalCommentChange(event) {
    this.setState({ personalComment: event.target.value});
    console.log(this.state.personalComment);
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                Doctor Profile
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  <FormGroup row>
                    <Col md="3">
                      <Label>User Name</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <p className="form-control-static">{this.state.username}</p>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="password-input">Password</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input onChange={this.handlePasswordChange} type="password" id="password-input" name="password-input" placeholder="Password"/>
                      <FormText className="help-block">Please enter a complex password</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="email-input">Email</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input onChange={this.handleEmailChange} type="email" id="email-input" name="email-input" placeholder="Enter Email"/>
                      <FormText className="help-block">Please enter your email</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label>Gender</Label>
                    </Col>
                    <Col md="9">
                      <FormGroup check inline>
                        <Input onChange={this.handleGenderChange} className="form-check-input" type="radio" id="inline-radio1" name="inline-radios" value="Male"/>
                        <Label className="form-check-label" check htmlFor="inline-radio1">Male</Label>
                      </FormGroup>
                      <FormGroup check inline>
                        <Input onChange={this.handleGenderChange} className="form-check-input" type="radio" id="inline-radio2" name="inline-radios" value="Female"/>
                        <Label className="form-check-label" check htmlFor="inline-radio2">Female</Label>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Hospital</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input onChange={this.handleHospitalChange} type="text" id="text-input" name="text-input" placeholder="Enter Hospital"/>
                      <FormText className="help-block">Please enter your hospital</FormText>
                    </Col>
                  </FormGroup>  
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="textarea-input">Personal Comment</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input onChange={this.handlePersonalCommentChange} type="textarea" name="textarea-input" id="textarea-input" rows="9"
                             placeholder="Content..."/>
                    </Col>
                  </FormGroup>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" onClick={this.updateChanges} size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Settings;
